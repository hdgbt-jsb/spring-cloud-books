package com.wujunshen.web.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: frankwoo(吴峻申) <br>
 * @date: 2017/12/19 <br>
 * @time: 21:06 <br>
 * @mail: frank_wjs@hotmail.com <br>
 */
@RestController
@RefreshScope
public class ConfigClientController {
    @Value("${spring.datasource.password}")
    private String db;
    @Value("${spring.rabbitmq.password}")
    private String rabbit;

    @Value("${profile}")
    private String profile;

    @GetMapping("/db")
    public String db() {
        return this.db;
    }

    @GetMapping("/rabbit")
    public String rabbit() {
        return this.rabbit;
    }

    @GetMapping("/profile")
    public String hello() {
        return this.profile;
    }
}